﻿using System;
using System.Windows.Forms;

namespace Calc
{
    /// <summary>
    /// Main Calc program class
    /// </summary>
    static class CalcProgram
    {
        /// <summary>
        /// Entry point for Calc program
        /// </summary>
        [STAThread]
        static void Main()
        {
            try
            {
                Application.Run(new MainForm());
            }
            // Main exception handler
            catch (Exception)
            {
                MessageBox.Show("Unknown error occured during Calc launch.", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
