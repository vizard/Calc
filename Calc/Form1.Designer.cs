﻿namespace Calc
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.numberOne = new System.Windows.Forms.Button();
            this.screen = new System.Windows.Forms.TextBox();
            this.numberTwo = new System.Windows.Forms.Button();
            this.nemberThree = new System.Windows.Forms.Button();
            this.numberFour = new System.Windows.Forms.Button();
            this.numberFive = new System.Windows.Forms.Button();
            this.numberSix = new System.Windows.Forms.Button();
            this.numberSeven = new System.Windows.Forms.Button();
            this.numberEight = new System.Windows.Forms.Button();
            this.numberNine = new System.Windows.Forms.Button();
            this.point = new System.Windows.Forms.Button();
            this.equal = new System.Windows.Forms.Button();
            this.numberZero = new System.Windows.Forms.Button();
            this.memoryPlus = new System.Windows.Forms.Button();
            this.memoryMinus = new System.Windows.Forms.Button();
            this.memotyRead = new System.Windows.Forms.Button();
            this.reset = new System.Windows.Forms.Button();
            this.subtraction = new System.Windows.Forms.Button();
            this.addition = new System.Windows.Forms.Button();
            this.multiplication = new System.Windows.Forms.Button();
            this.division = new System.Windows.Forms.Button();
            this.buttonsTips = new System.Windows.Forms.ToolTip(this.components);
            this.layoutButtonsPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.layoutButtonsPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // numberOne
            // 
            this.numberOne.Location = new System.Drawing.Point(3, 3);
            this.numberOne.Name = "numberOne";
            this.numberOne.Size = new System.Drawing.Size(40, 40);
            this.numberOne.TabIndex = 2;
            this.numberOne.Tag = "1";
            this.numberOne.Text = "1";
            this.numberOne.UseVisualStyleBackColor = true;
            this.numberOne.Click += new System.EventHandler(this.NumberClicked);
            // 
            // screen
            // 
            this.screen.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.screen.Location = new System.Drawing.Point(12, 12);
            this.screen.Name = "screen";
            this.screen.ReadOnly = true;
            this.screen.Size = new System.Drawing.Size(230, 20);
            this.screen.TabIndex = 0;
            this.screen.TabStop = false;
            // 
            // numberTwo
            // 
            this.numberTwo.Location = new System.Drawing.Point(49, 3);
            this.numberTwo.Name = "numberTwo";
            this.numberTwo.Size = new System.Drawing.Size(40, 40);
            this.numberTwo.TabIndex = 3;
            this.numberTwo.Tag = "2";
            this.numberTwo.Text = "2";
            this.numberTwo.UseVisualStyleBackColor = true;
            this.numberTwo.Click += new System.EventHandler(this.NumberClicked);
            // 
            // nemberThree
            // 
            this.nemberThree.Location = new System.Drawing.Point(95, 3);
            this.nemberThree.Name = "nemberThree";
            this.nemberThree.Size = new System.Drawing.Size(40, 40);
            this.nemberThree.TabIndex = 4;
            this.nemberThree.Tag = "3";
            this.nemberThree.Text = "3";
            this.nemberThree.UseVisualStyleBackColor = true;
            this.nemberThree.Click += new System.EventHandler(this.NumberClicked);
            // 
            // numberFour
            // 
            this.numberFour.Location = new System.Drawing.Point(3, 49);
            this.numberFour.Name = "numberFour";
            this.numberFour.Size = new System.Drawing.Size(40, 40);
            this.numberFour.TabIndex = 5;
            this.numberFour.Tag = "4";
            this.numberFour.Text = "4";
            this.numberFour.UseVisualStyleBackColor = true;
            this.numberFour.Click += new System.EventHandler(this.NumberClicked);
            // 
            // numberFive
            // 
            this.numberFive.Location = new System.Drawing.Point(49, 49);
            this.numberFive.Name = "numberFive";
            this.numberFive.Size = new System.Drawing.Size(40, 40);
            this.numberFive.TabIndex = 6;
            this.numberFive.Tag = "5";
            this.numberFive.Text = "5";
            this.numberFive.UseVisualStyleBackColor = true;
            this.numberFive.Click += new System.EventHandler(this.NumberClicked);
            // 
            // numberSix
            // 
            this.numberSix.Location = new System.Drawing.Point(95, 49);
            this.numberSix.Name = "numberSix";
            this.numberSix.Size = new System.Drawing.Size(40, 40);
            this.numberSix.TabIndex = 7;
            this.numberSix.Tag = "6";
            this.numberSix.Text = "6";
            this.numberSix.UseVisualStyleBackColor = true;
            this.numberSix.Click += new System.EventHandler(this.NumberClicked);
            // 
            // numberSeven
            // 
            this.numberSeven.Location = new System.Drawing.Point(3, 95);
            this.numberSeven.Name = "numberSeven";
            this.numberSeven.Size = new System.Drawing.Size(40, 40);
            this.numberSeven.TabIndex = 8;
            this.numberSeven.Tag = "7";
            this.numberSeven.Text = "7";
            this.numberSeven.UseVisualStyleBackColor = true;
            this.numberSeven.Click += new System.EventHandler(this.NumberClicked);
            // 
            // numberEight
            // 
            this.numberEight.Location = new System.Drawing.Point(49, 95);
            this.numberEight.Name = "numberEight";
            this.numberEight.Size = new System.Drawing.Size(40, 40);
            this.numberEight.TabIndex = 9;
            this.numberEight.Tag = "8";
            this.numberEight.Text = "8";
            this.numberEight.UseVisualStyleBackColor = true;
            this.numberEight.Click += new System.EventHandler(this.NumberClicked);
            // 
            // numberNine
            // 
            this.numberNine.Location = new System.Drawing.Point(95, 95);
            this.numberNine.Name = "numberNine";
            this.numberNine.Size = new System.Drawing.Size(40, 40);
            this.numberNine.TabIndex = 10;
            this.numberNine.Tag = "9";
            this.numberNine.Text = "9";
            this.numberNine.UseVisualStyleBackColor = true;
            this.numberNine.Click += new System.EventHandler(this.NumberClicked);
            // 
            // point
            // 
            this.point.Location = new System.Drawing.Point(49, 141);
            this.point.Name = "point";
            this.point.Size = new System.Drawing.Size(40, 40);
            this.point.TabIndex = 11;
            this.point.Text = ".";
            this.point.UseVisualStyleBackColor = true;
            this.point.Click += new System.EventHandler(this.PointClicked);
            // 
            // equal
            // 
            this.equal.Location = new System.Drawing.Point(95, 141);
            this.equal.Name = "equal";
            this.equal.Size = new System.Drawing.Size(40, 40);
            this.equal.TabIndex = 16;
            this.equal.Text = "=";
            this.equal.UseVisualStyleBackColor = true;
            this.equal.Click += new System.EventHandler(this.OperationClicked);
            // 
            // numberZero
            // 
            this.numberZero.Location = new System.Drawing.Point(3, 141);
            this.numberZero.Name = "numberZero";
            this.numberZero.Size = new System.Drawing.Size(40, 40);
            this.numberZero.TabIndex = 1;
            this.numberZero.Tag = "0";
            this.numberZero.Text = "0";
            this.numberZero.UseVisualStyleBackColor = true;
            this.numberZero.Click += new System.EventHandler(this.NumberClicked);
            // 
            // memoryPlus
            // 
            this.memoryPlus.Location = new System.Drawing.Point(187, 49);
            this.memoryPlus.Name = "memoryPlus";
            this.memoryPlus.Size = new System.Drawing.Size(40, 40);
            this.memoryPlus.TabIndex = 18;
            this.memoryPlus.Tag = "Add";
            this.memoryPlus.Text = "M+";
            this.memoryPlus.UseVisualStyleBackColor = true;
            this.memoryPlus.Click += new System.EventHandler(this.MemoryClicked);
            this.memoryPlus.MouseHover += new System.EventHandler(this.Point_MouseEnter);
            // 
            // memoryMinus
            // 
            this.memoryMinus.Location = new System.Drawing.Point(187, 95);
            this.memoryMinus.Name = "memoryMinus";
            this.memoryMinus.Size = new System.Drawing.Size(40, 40);
            this.memoryMinus.TabIndex = 19;
            this.memoryMinus.Tag = "Sub";
            this.memoryMinus.Text = "M-";
            this.memoryMinus.UseVisualStyleBackColor = true;
            this.memoryMinus.Click += new System.EventHandler(this.MemoryClicked);
            this.memoryMinus.MouseHover += new System.EventHandler(this.Point_MouseEnter);
            // 
            // memotyRead
            // 
            this.memotyRead.Location = new System.Drawing.Point(187, 141);
            this.memotyRead.Name = "memotyRead";
            this.memotyRead.Size = new System.Drawing.Size(40, 40);
            this.memotyRead.TabIndex = 20;
            this.memotyRead.Tag = "Rea";
            this.memotyRead.Text = "MR";
            this.memotyRead.UseVisualStyleBackColor = true;
            this.memotyRead.Click += new System.EventHandler(this.MemoryClicked);
            this.memotyRead.MouseHover += new System.EventHandler(this.Point_MouseEnter);
            // 
            // reset
            // 
            this.reset.Location = new System.Drawing.Point(187, 3);
            this.reset.Name = "reset";
            this.reset.Size = new System.Drawing.Size(40, 40);
            this.reset.TabIndex = 17;
            this.reset.Tag = "reset";
            this.reset.Text = "C";
            this.reset.UseVisualStyleBackColor = true;
            this.reset.Click += new System.EventHandler(this.ResetClicked);
            this.reset.MouseHover += new System.EventHandler(this.Point_MouseEnter);
            // 
            // subtraction
            // 
            this.subtraction.Location = new System.Drawing.Point(141, 49);
            this.subtraction.Name = "subtraction";
            this.subtraction.Size = new System.Drawing.Size(40, 40);
            this.subtraction.TabIndex = 13;
            this.subtraction.Text = "-";
            this.subtraction.UseVisualStyleBackColor = true;
            this.subtraction.Click += new System.EventHandler(this.OperationClicked);
            // 
            // addition
            // 
            this.addition.Location = new System.Drawing.Point(141, 3);
            this.addition.Name = "addition";
            this.addition.Size = new System.Drawing.Size(40, 40);
            this.addition.TabIndex = 12;
            this.addition.Text = "+";
            this.addition.UseVisualStyleBackColor = true;
            this.addition.Click += new System.EventHandler(this.OperationClicked);
            // 
            // multiplication
            // 
            this.multiplication.Location = new System.Drawing.Point(141, 95);
            this.multiplication.Name = "multiplication";
            this.multiplication.Size = new System.Drawing.Size(40, 40);
            this.multiplication.TabIndex = 14;
            this.multiplication.Text = "*";
            this.multiplication.UseVisualStyleBackColor = true;
            this.multiplication.Click += new System.EventHandler(this.OperationClicked);
            // 
            // division
            // 
            this.division.Location = new System.Drawing.Point(141, 141);
            this.division.Name = "division";
            this.division.Size = new System.Drawing.Size(40, 40);
            this.division.TabIndex = 15;
            this.division.Text = "/";
            this.division.UseVisualStyleBackColor = true;
            this.division.Click += new System.EventHandler(this.OperationClicked);
            // 
            // layoutButtonsPanel
            // 
            this.layoutButtonsPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.layoutButtonsPanel.Controls.Add(this.numberOne);
            this.layoutButtonsPanel.Controls.Add(this.numberTwo);
            this.layoutButtonsPanel.Controls.Add(this.nemberThree);
            this.layoutButtonsPanel.Controls.Add(this.addition);
            this.layoutButtonsPanel.Controls.Add(this.reset);
            this.layoutButtonsPanel.Controls.Add(this.numberFour);
            this.layoutButtonsPanel.Controls.Add(this.numberFive);
            this.layoutButtonsPanel.Controls.Add(this.numberSix);
            this.layoutButtonsPanel.Controls.Add(this.subtraction);
            this.layoutButtonsPanel.Controls.Add(this.memoryPlus);
            this.layoutButtonsPanel.Controls.Add(this.numberSeven);
            this.layoutButtonsPanel.Controls.Add(this.numberEight);
            this.layoutButtonsPanel.Controls.Add(this.numberNine);
            this.layoutButtonsPanel.Controls.Add(this.multiplication);
            this.layoutButtonsPanel.Controls.Add(this.memoryMinus);
            this.layoutButtonsPanel.Controls.Add(this.numberZero);
            this.layoutButtonsPanel.Controls.Add(this.point);
            this.layoutButtonsPanel.Controls.Add(this.equal);
            this.layoutButtonsPanel.Controls.Add(this.division);
            this.layoutButtonsPanel.Controls.Add(this.memotyRead);
            this.layoutButtonsPanel.Location = new System.Drawing.Point(12, 38);
            this.layoutButtonsPanel.Name = "layoutButtonsPanel";
            this.layoutButtonsPanel.Size = new System.Drawing.Size(230, 185);
            this.layoutButtonsPanel.TabIndex = 21;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(254, 225);
            this.Controls.Add(this.layoutButtonsPanel);
            this.Controls.Add(this.screen);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.Text = "Calc";
            this.layoutButtonsPanel.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion


        private System.Windows.Forms.TextBox screen;
        private System.Windows.Forms.Button numberZero;
        private System.Windows.Forms.Button numberOne;
        private System.Windows.Forms.Button numberTwo;
        private System.Windows.Forms.Button nemberThree;
        private System.Windows.Forms.Button numberFour;
        private System.Windows.Forms.Button numberFive;
        private System.Windows.Forms.Button numberSix;
        private System.Windows.Forms.Button numberSeven;
        private System.Windows.Forms.Button numberEight;
        private System.Windows.Forms.Button numberNine;
        private System.Windows.Forms.Button point;
        private System.Windows.Forms.Button equal;
        private System.Windows.Forms.Button memoryPlus;
        private System.Windows.Forms.Button memoryMinus;
        private System.Windows.Forms.Button memotyRead;
        private System.Windows.Forms.Button reset;
        private System.Windows.Forms.Button addition;
        private System.Windows.Forms.Button subtraction;
        private System.Windows.Forms.Button multiplication;
        private System.Windows.Forms.Button division;
        private System.Windows.Forms.ToolTip buttonsTips;
        private System.Windows.Forms.FlowLayoutPanel layoutButtonsPanel;
    }
}

