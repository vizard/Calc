﻿/// Decimal simple calculator inmplementation for Windowws Forms
/// Victor K. 2018
/// TODO:
/// * check for overflow
/// * implement floats

using System;
using System.Windows.Forms;

namespace Calc
{

    enum Operations {
        Addition,
        Subtraction,
        Multiplication,
        Division,
        None
    };

    public partial class MainForm : Form
    {
        private double screenNumber;
        /// <summary>
        /// Get: getting screenNumber variable value
        /// Set: setting new string value for screen.
        /// </summary>
        private double ScreenNumber
        {
            get
            {
                return resultDisplayed ? 0 : screenNumber;
            }
            set
            {
                screenNumber = value;
                screen.Text = infoString + Convert.ToString(screenNumber);
            }
        }

        private double resultOperand, secondOperand;
        private double memoryOperand;

        private double MemoryOperand
        {
            get
            {
                return memoryOperand;
            }
            set
            {
                memoryOperand = value;
                if (memoryOperand != 0)
                {
                    this.Text += " (memory set)";
                }
                else
                {
                    this.Text = "Calc";
                } 
            }
        }

        private bool isDecimal;
        private bool IsDecimal
        {
            get
            {
                return isDecimal;
            }
            set
            {
                isDecimal = value;
                point.Enabled = !isDecimal;
            }
        }

        bool resultDisplayed;
        string infoString;
        private Operations operation;

        public MainForm()
        {
            InitializeComponent();
            ResetCalculator();
            memoryOperand = 0;
        }

        /// <summary>
        /// Sets all program variables to their default value.
        /// </summary>
        private void ResetCalculator()
        {
            resultOperand = 0;
            secondOperand = 0;
            ScreenNumber = resultOperand;
            infoString = "";
            isDecimal = false;
            resultDisplayed = false;
            operation = Operations.None;
        }

        /// <summary>
        /// Event handler for number button click.
        /// </summary>
        /// <param name="sender">Pressed button</param>
        /// <param name="e"></param>
        private void NumberClicked(object sender, EventArgs e)
        {
            if (resultDisplayed)
            {
                resultDisplayed = false;
                ScreenNumber = 0;
            }
            Button pressedNumberButton = (Button)sender;
            ScreenNumber = ScreenNumber * 10 + Convert.ToDouble(pressedNumberButton.Tag);
        }

        /// <summary>
        /// Event handler for reset button click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ResetClicked(object sender, EventArgs e) => ResetCalculator();

        /// <summary>
        /// Event hadler for memory operation button clock.
        /// </summary>
        /// <param name="sender">Pressed button</param>
        /// <param name="e"></param>
        private void MemoryClicked(object sender, EventArgs e)
        {
            Button pressedButton = (Button)sender;
            switch (pressedButton.Tag)
            {
                case "Add":
                    MemoryOperand += ScreenNumber;
                    break;
                case "Sub":
                    MemoryOperand -= ScreenNumber;
                    break;
                case "Rea":
                    ScreenNumber = MemoryOperand;
                    resultDisplayed = false;
                    break;
            }
           
        }

        private void PointClicked(object sender, EventArgs e)
        {
            //if (!isDecimal)
            //{
            //     screen.Text += ".";
            //     ScreenNumber = Convert.ToDouble(Convert.ToString(ScreenNumber) + ".0", CultureInfo.InvariantCulture);
            //}
        }

        /// <summary>
        /// Event handler for arithmetic operation button click.
        /// </summary>
        /// <param name="sender">Pressed button</param>
        /// <param name="e"></param>
        private void OperationClicked(object sender, EventArgs e)
        {
            Button pressedActionButton = (Button)sender;
            PerformOperation(pressedActionButton.Text); 
        }

        private void Point_MouseEnter(object sender, EventArgs e)
        {
            Button enteredButton = (Button)sender;
            string message = null;
            switch(enteredButton.Tag)
            {
                case "reset":
                    message = "Reset calculator";
                    break;
                case "Add":
                    message = "Add value to memory";
                    break;
                case "Sub":
                    message = "Subtract value from memory";
                    break;
                case "Rea":
                    message = "Print memory value";
                    break;
            }
            buttonsTips.Show(message, enteredButton);
        }

        private void PerformOperation(string oper)
        {
            if (operation == Operations.None)
            {
                resultOperand = ScreenNumber;
            }
            switch (oper)
            {
                case "+":
                    Action(Operations.Addition);
                    break;
                case "-":
                    Action(Operations.Subtraction);
                    break;
                case "*":
                    Action(Operations.Multiplication);
                    break;
                case "/":
                    Action(Operations.Division);
                    break;
                case "=":
                    if (!resultDisplayed)
                    {
                        secondOperand = ScreenNumber;
                    }
                    switch (operation)
                    {
                        case Operations.Addition:
                            resultOperand += secondOperand;
                            break;
                        case Operations.Subtraction:
                            resultOperand -= secondOperand;
                            break;
                        case Operations.Multiplication:
                            resultOperand *= secondOperand;
                            break;
                        case Operations.Division:
                            resultOperand /= secondOperand;
                            break;
                    }
                    ScreenNumber = resultOperand;
                    break;
            }
            resultDisplayed = true;
        }

        private void Action(Operations oper)
        {
            if (operation != Operations.None)
            {
                secondOperand = ScreenNumber;

                switch (operation)
                {
                    case Operations.Addition:
                        resultOperand += secondOperand;
                        break;
                    case Operations.Subtraction:
                        resultOperand -= secondOperand;
                        break;
                    case Operations.Multiplication:
                        resultOperand *= secondOperand;
                        break;
                    case Operations.Division:
                        resultOperand /= secondOperand;
                        break;
                }
            }
            if (operation != oper)
            {
                secondOperand = 0;
            }
            else
            {
                secondOperand = ScreenNumber;
                switch(oper)
                {
                    case Operations.Addition:
                        resultOperand += ScreenNumber;
                        break;
                    case Operations.Subtraction:
                        resultOperand -= ScreenNumber;
                        break;
                    case Operations.Multiplication:
                        resultOperand *= ScreenNumber;
                        break;
                    case Operations.Division:
                        resultOperand /= ScreenNumber;
                        break;
                }
                ScreenNumber = resultOperand;
            }
            operation = oper;
        }
    }
}